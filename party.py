#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields

class Address(ModelSQL, ModelView):
    _name = 'party.address'

    list_address = fields.Function(fields.Text('Address List'), 'get_list_address')

    def get_list_address(self, ids, name):
        if not ids:
            return {}
        res = {}
        for address in self.browse(ids):
            res[address.id] = ''
            if address.street:
                res[address.id] += address.street
            if address.streetbis:
                if res[address.id]:
                    res[address.id] += '\n'
                res[address.id] += address.streetbis
            if address.zip or address.city:
                if res[address.id]:
                    res[address.id] += '\n'
                if address.zip:
                    res[address.id] += address.zip
                if address.city:
                    if res[address.id][-1:] != '\n':
                        res[address.id] += ' '
                    res[address.id] += address.city
            if address.country or address.subdivision:
                if res[address.id]:
                    res[address.id] += '\n'
                if address.subdivision:
                    res[address.id] += address.subdivision.name
                if address.country:
                    if res[address.id][-1:] != '\n':
                        res[address.id] += ' '
                    res[address.id] += address.country.name
        return res

Address()
