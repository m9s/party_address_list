#This file is part of Tryton.  The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
{
    'name': 'Party Address List',
    'name_de_DE': 'Parteien Adressenliste',
    'version': '2.2.0',
    'author': 'virtual things',
    'email': 'info@virtual-things.biz',
    'website': 'http://www.virtual-things.biz',
    'description': '''
    - Adds an address list to parties
''',
    'description_de_DE': '''
    - Fügt eine Adressenliste zu Parteien hinzu
''',
    'depends' : [
        'party',
        'party_type',
        'party_contact_mechanism_intended_use',
    ],
    'xml' : [
        'party.xml',
    ],
    'translation': [
        'locale/de_DE.po',
    ],
}
